import React from "react";
import Authentication from "./Authentication";
import AuthenticationService from "../services/AuthenticationService";
import { Redirect } from "react-router-dom";


class SignUp extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            redirectIfLoggedIn: false
        }
    }

    componentDidMount(){
        if(AuthenticationService.isLoggedIn()){
            this.setState({redirectIfLoggedIn: true});
        }
    }

    render(){
        if (this.state.redirectIfLoggedIn) return <Redirect to ="/"/>

        return(
                <div style={{textAlign: 'center'}} className="container">
                <h1>Sign Up</h1>
                <Authentication history ={this.props.history} endpoint = "/authentication/createuser" button='Sign Up' />
                </div>
        )
    }
}

export default SignUp;